import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-administrator-page',
  templateUrl: './administrator-page.component.html',
  styleUrls: ['./administrator-page.component.css'],
})
export class AdministratorPageComponent {
  administrators: Array<any> = [
    {
      id: '1',
      firstName: 'Juan',
      lastName: 'Marin',
      gender: 'Hombre',
      birthDate: '31/10/2001',
      deleted: 'N',
    },
    {
      id: '2',
      firstName: 'Ignacio',
      lastName: 'Marin',
      gender: 'Hombre',
      birthDate: '07/05/1997',
      deleted: 'N',
    },
  ];

  form: FormGroup;

  constructor(private fb: FormBuilder) {
    this.form = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      gender: ['', Validators.required],
      birthDate: ['', Validators.required],
    });
  }

  onSubmit() {
    if (this.form.valid) {
      console.log(this.form.value);
      //this.administrators.push.apply(this.administrators.length+1,this.form.value.firstName,this.form.value.firstName,this.form.value.gender,this.form.value.birthDate);
    }
  }

  modifyAdmin(id: number) {}

  deleteAdmin(id: number) {
    for (let index = 0; index < this.administrators.length; index++) {
      if (this.administrators[index].id == id) {
        this.administrators[index].deleted = 'Y';
      }
    }
  }

  restoreAdmin(id: number) {
    for (let index = 0; index < this.administrators.length; index++) {
      if (this.administrators[index].id == id) {
        this.administrators[index].deleted = 'N';
      }
    }
  }

  deleteAdminID(id: number) {
    for (let index = 0; index < this.administrators.length; index++) {
      if (this.administrators[index].id == id) {
        this.administrators.splice(index, id);
      }
    }
  }
}
