import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomePageComponent } from './components/pages/home-page/home-page.component';
import { OfficePageComponent } from './components/pages/office-page/office-page.component';
import { AdministratorPageComponent } from './components/pages/administrator-page/administrator-page.component';

const routes: Routes = [
  { path: 'home', component: HomePageComponent },
  { path: 'office', component: OfficePageComponent },
  { path: 'admin', component: AdministratorPageComponent },
  { path: '**', redirectTo: 'home' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
